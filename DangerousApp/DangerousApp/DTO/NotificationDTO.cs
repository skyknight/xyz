﻿using DangerousApp.Core;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DangerousApp.DTO
{
    public class NotificationDTO
    {
        public string Category { get; set; }

        public string City { get; set; }

        public string Subcategory { get; set; }

        public string NotificationType { get; set; }

        [JsonConverter(typeof(UnixDateTimeMilisecondsConverter))]
        public DateTime CreateDate { get; set; }

        public string Event { get; set; }

        public string Source { get; set; }

        public IEnumerable<StatusDTO> Statuses { get; set; } = new List<StatusDTO>();
    }
}
