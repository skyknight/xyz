﻿using DangerousApp.Core;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DangerousApp.DTO
{
    public class StatusDTO
    {
        public string Status { get; set; }

        public string Description { get; set; }

        [JsonConverter(typeof(UnixDateTimeMilisecondsConverter))]
        public DateTime ChangeDate { get; set; }
    }
}
