﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DangerousApp.DTO;
using DangerousApp.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace DangerousApp.Pages
{
    public class IndexModel : PageModel
    {
        private readonly INotificationsService _notificationsService;

        public IEnumerable<NotificationDTO> Notifications { get; set; }

        public DateTime Day { get; set; } = DateTime.Today;

        public IndexModel(INotificationsService notificationsService)
        {
            _notificationsService = notificationsService;
        }

        public async Task OnGetAsync()
        {
            Notifications = await _notificationsService.GetNotifications(Day);
        }
    }
}
