﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DangerousApp.DTO;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;

namespace DangerousApp.Services
{
    public class WarsawNotificationService : INotificationsService
    {
        private readonly IRestClient _restClient;
        private readonly string _apiKey;
        private readonly JsonSerializer _jsonSerializer = new JsonSerializer();

        public WarsawNotificationService(IOptions<WarsawNotificationServiceSettings> options)
        {
            _restClient = new RestClient(options.Value.ApiUrl);
            _apiKey = options.Value.ApiKey;
        }

        public async Task<IEnumerable<NotificationDTO>> GetNotifications(DateTime day)
        {
            var request = new RestRequest("action/19115store_getNotificationsForDate", Method.GET);
            request.AddQueryParameter("id", "28dc65ad-fff5-447b-99a3-95b71b4a7d1e");
            request.AddQueryParameter("apikey", _apiKey);
            request.AddQueryParameter("dateFrom", new DateTimeOffset(day.Date).ToUnixTimeMilliseconds().ToString());
            request.AddQueryParameter("dateTo", new DateTimeOffset(day.Date.AddDays(1)).ToUnixTimeMilliseconds().ToString());

            var response = await _restClient.ExecuteTaskAsync(request);
            
            if (response.ErrorException != null)
            {
                throw new Exception("Wystąpił błąd podczas wywołania API.", response.ErrorException);
            }

            if ((int)response.StatusCode >= 400)
            {
                throw new Exception($"Wystąpił błąd podczas wywołania API: {response.StatusCode}, {response.StatusDescription}.");
            }

            var jsonDocument = JObject.Parse(response.Content);

            if (jsonDocument.SelectToken("$.result.success")?.Value<bool>() != true)
            {
                throw new Exception("Wystąpił błąd podczas wywołania API.");
            }

            using (var jsonReader = new JTokenReader(jsonDocument.SelectToken("$.result.result.notifications")))
            {
                return _jsonSerializer.Deserialize<IEnumerable<NotificationDTO>>(jsonReader);
            }
        }
    }
}
