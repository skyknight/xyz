﻿using DangerousApp.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DangerousApp.Services
{
    public interface INotificationsService
    {
        Task<IEnumerable<NotificationDTO>> GetNotifications(DateTime day);
    }
}
