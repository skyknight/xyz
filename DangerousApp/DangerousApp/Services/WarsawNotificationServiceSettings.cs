﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DangerousApp.Services
{
    public class WarsawNotificationServiceSettings
    {
        public string ApiUrl { get; set; }

        public string ApiKey { get; set; }
    }
}
