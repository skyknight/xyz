﻿param(
    [Parameter(Mandatory=$True)][string]$resourceGroupName,
    [string[]]$resourceTypes=@("Microsoft.Web/sites","microsoft.sql/servers/databases")
)

Foreach($resourceType in $resourceTypes)
{
    $resources = Get-AzResource -ResourceGroupName $resourceGroupName -ResourceType $resourceType

    Foreach($resource in $resources)
    {
        Write-Host "Removing " $resource.ResourceId
        Remove-AzResource -ResourceId $resource.ResourceId -Force
    }
}